package com.carolynvs.stash.plugin.force_field;

import java.util.Map;
import java.util.HashMap;
import java.io.IOException;

import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.common.collect.ImmutableMap;

import javax.servlet.ServletException;

import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.bitbucket.nav.NavBuilder;

/**
 * @author Jim Bethancourt
 */
public class ForceFieldConfigServlet extends HttpServlet {

    static final String SETTINGS_MAP = "com.carolynvs.force-field.settings";

    private static final Logger log = LoggerFactory.getLogger(ForceFieldConfigServlet.class);
    final private SoyTemplateRenderer soyTemplateRenderer;
    private final NavBuilder navBuilder;
    private Map<String, String> fields;
    private Map<String, Iterable<String>> fieldErrors;
    private final PluginSettings pluginSettings;
    private HashMap<String, String> settingsMap;

    public ForceFieldConfigServlet(SoyTemplateRenderer soyTemplateRenderer,
                                   PluginSettingsFactory pluginSettingsFactory,
                                   NavBuilder navBuilder) {
        this.soyTemplateRenderer = soyTemplateRenderer;
        this.navBuilder = navBuilder;

        pluginSettings = pluginSettingsFactory.createGlobalSettings();

        fields = new HashMap<>();
        fieldErrors = new HashMap<>();
    }


    @SuppressWarnings("unchecked")
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        log.debug("doGet");
        settingsMap = (HashMap<String, String>) pluginSettings.get(SETTINGS_MAP);
        if (settingsMap == null) {
            settingsMap = new HashMap<>();
        }

        doGetContinue(req, resp);
    }

    protected void doGetContinue(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        log.debug("doGetContinue");
        fields.clear();

        for (Map.Entry<String, String> entry : settingsMap.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            log.debug("got plugin config " + key + "=" + value + " " + value.getClass().getName());
            fields.put(key, value);
        }

        log.debug("Config fields: " + fields);
        log.debug("Field errors: " + fieldErrors);

        resp.setContentType("text/html;charset=UTF-8");
        try {
            soyTemplateRenderer.render(resp.getWriter(), "com.carolynvs.force-field:forceField-config-serverside", "com.carolynvs.stash.plugin.force_field.config",
                    ImmutableMap
                            .<String, Object>builder()
                            .put("config", fields)
                            .put("errors", fieldErrors)
                            .build()
            );
        } catch (SoyException e) {
            Throwable cause = e.getCause();
            if (cause instanceof IOException) {
                throw (IOException) cause;
            }
            throw new ServletException(e);
        }

    }

    void addStringFieldValue(Map<String, String> settingsMap, HttpServletRequest req, String fieldName) {
        String value = req.getParameter(fieldName);
        if (value != null && !value.isEmpty()) settingsMap.put(fieldName, value);
    }

    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        settingsMap.clear();

        // Plugin globalConfig persister supports only map of strings
        for (String parameterName : req.getParameterMap().keySet()) {

            // Plugin settings persister only supports map of strings
            if (!parameterName.startsWith("errorMessage") && !parameterName.equals("submit")) {
                addStringFieldValue(settingsMap, req, parameterName);
            }
        }

        if (fieldErrors.size() > 0) {
            doGetContinue(req, resp);
            return;
        }

        if(log.isDebugEnabled()) {
            for (Map.Entry<String, String> entry : settingsMap.entrySet()) {
                String key = entry.getKey();
                Object value = entry.getValue();
                log.debug("save plugin config " + key + "=" + value + " " + value.getClass().getName());
            }
        }

        pluginSettings.put(SETTINGS_MAP, settingsMap);

        String redirectUrl;
        redirectUrl = navBuilder.addons().buildRelative();
        log.debug("redirect: " + redirectUrl);
        resp.sendRedirect(redirectUrl);
    }
}
