package com.carolynvs.stash.plugin.force_field;

import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.atlassian.bitbucket.hook.repository.RepositoryHookService;
import com.atlassian.bitbucket.nav.NavBuilder;
import com.atlassian.bitbucket.setting.Settings;
import com.atlassian.bitbucket.setting.SettingsBuilder;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

/**
 * @author Jim Bethancourt
 */
public class ForceFieldConfigServletTest {

    @Mock private SoyTemplateRenderer soyTemplateRenderer;
    @Mock private PluginSettingsFactory pluginSettingsFactory;
    @Mock private PluginSettings pluginSettings;
    @Mock private NavBuilder navBuilder;
    @Mock private NavBuilder.Addons addons;
    @Mock private SettingsBuilder settingsBuilder;
    @Mock private Settings settings;

    @Mock private HttpServletRequest request;
    @Mock private HttpServletResponse response;

    ForceFieldConfigServlet forceFieldConfigServlet;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        when(pluginSettingsFactory.createGlobalSettings()).thenReturn(pluginSettings);
        when(pluginSettings.get(anyString())).thenReturn(new HashMap<String, String>());

        when(settingsBuilder.build()).thenReturn(settings);
        when(settingsBuilder.addAll(anyMap())).thenReturn(settingsBuilder);

        when(navBuilder.addons()).thenReturn(addons);
        when(addons.buildRelative()).thenReturn("/forceField/config");

        forceFieldConfigServlet = new ForceFieldConfigServlet(soyTemplateRenderer,
                pluginSettingsFactory, navBuilder);
    }

    @Test
    public void testDoGet() throws IOException, ServletException, SoyException {
        forceFieldConfigServlet.doGet(request, response);
        HashMap<String, Object> map = new HashMap<>();
        map.put("config", new HashMap());
        map.put("errors", new HashMap());
        verify(soyTemplateRenderer, times(1)).render(null,
                "com.carolynvs.force-field:forceField-config-serverside", "com.carolynvs.stash.plugin.force_field.config", map);
    }

    @Test
    public void testDoPostNoParams() throws IOException, ServletException {
        forceFieldConfigServlet.doGet(request, response); // calling doGet to populate the settings map
        forceFieldConfigServlet.doPost(request, response);
        verify(response, times(1)).sendRedirect("/forceField/config");
    }

    @Test
    public void testDoPostWithParams() throws IOException, ServletException {
        Map<String, String[]> parameterMap = new HashMap<>();
        parameterMap.put("references", new String[]{"master"});
        when(request.getParameterMap()).thenReturn(parameterMap);

        Map<String, String> settingsMap = new HashMap<>();
        settingsMap.put("references", "master");
        when(pluginSettings.get(anyString())).thenReturn(settingsMap);

        forceFieldConfigServlet = new ForceFieldConfigServlet(soyTemplateRenderer,
                pluginSettingsFactory, navBuilder);

        forceFieldConfigServlet.doGet(request, response); // calling doGet to populate the settings map
        forceFieldConfigServlet.doPost(request, response);

        spy(forceFieldConfigServlet).addStringFieldValue(settingsMap, request, "references");
        verify(response, times(1)).sendRedirect("/forceField/config");
    }
}
